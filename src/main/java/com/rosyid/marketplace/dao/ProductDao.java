package com.rosyid.marketplace.dao;

import com.rosyid.marketplace.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
